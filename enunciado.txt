1. En esta carpeta tienes un index.html y un index.ts. Con ayuda del compilador de TypeScript (tsc),
genera un archivo index.js con el código correspondiente en ES5. El index.html ya está configurado para 
cargar el archivo index.js generado.

2. Abre el index.html con el Chrome y mira la consola del navegador para comprobar que el script 
funciona correctamente.

3. Divide el código que hay ahora en index.ts en tres módulos diferentes. Crea un módulo modColores.ts, 
que contendrá el array de colores, y lo exportará para que lo consuman otros módulos. Haz lo mismo con 
modTallas.ts. Crea después un tercer módulo modListar.ts, que contendrá la función para listar items, 
y la exportará para que la consuman otros módulos.

4. Modifica index.ts para que importe los arrays y la función de los tres módulos que has creado.

5. Con ayuda del compilador de TypeScript, genera los archivos js correspondientes a los cuatro archivos
que tienes en TypeScript.

6. Abre el index.html con el Chrome y mira la consola del navegador para comprobar si el script funciona 
correctamente. ¿Funciona? ¿Por qué?

7. Con ayuda de webpack, crea un bundle a partir de index.js. Llámalo bundle.js.

8. Modifica index.html para que llame a bundle.js en vez de a index.js.

9. Abre el index.html con el Chrome y mira la consola del navegador para comprobar si el script funciona 
correctamente.