let colores: Array<string> = ["rojo", "amarillo", "verde"];
let tallas: Array<string> = ["S", "L", "XL"];

function listaItems(items: Array<string>): string {
    return items.reduce( (c1, c2) => `${c1}, ${c2}`);
}

let txtColores = `Los colores disponibles son ${listaItems(colores)}.`;
console.log(txtColores);

let txtTallas = `Las tallas disponibles son ${listaItems(tallas)}.`;
console.log(txtTallas);