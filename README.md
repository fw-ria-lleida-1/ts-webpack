#Formación Frameworks RIA multiplataforma - Lleida

##Ejercicio TypeScript y Webpack
Ejercicio sobre la compilación de archivos TypeScript y utilización de Webpack para módulos.

###Cómo descargarla
Si quieres usar Git, copia la URL que tienes más arriba, al lado de **HTTPS**, y haz un **git clone** desde tu disco duro.

Si quieres descargarte un zip, ve a la opción **Downloads** del menú izquierdo.

Si es la primera vez que la descargas, o si se ha añadido algún cambio en el archivo package.json, recuerda lanzar **npm install** después de descargarla.